# mtls - Mutual TLS

mTLS demo using nginx in Docker

Use `cert.sh` to generate and sign CA and user certificates.  
Files generated
- key.pem Server Encrypted Keystore
- privkey.pem Server Unencrypted Keystore
- fullchain.pem Server Certificate
- user.key  User Private Key
- user.csr  User Certificate Signing Request 
- user.crt  Signed User Certificate 
- user.pfx  PFX Format User Certificate

Place the private key password inside a file called `pass` before running `docker build .`

Import user certificate user.pfx into the browser.

Clients with no certificates will see Error 403.
