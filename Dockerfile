FROM nginx:alpine
RUN apk add --no-cache bash
COPY nginx.conf /etc/nginx
COPY default.conf /etc/nginx/conf.d
RUN mkdir /etc/nginx/certs
COPY *.pem /etc/nginx/certs/
COPY pass /etc/nginx/certs/
