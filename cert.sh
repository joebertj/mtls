#!/bin/bash

echo "Creating the Certificate Authority..."
openssl genrsa -des3 -out key.pem 2048
echo "Unencrypting private key..."
openssl rsa -in key.pem -out privkey.pem
echo "Creating a CA Certificate..."
openssl req -new -x509 -days 365 -key privkey.pem -out fullchain.pem
echo "Creating a User Key..."
openssl genrsa -des3 -out user.key 2048
echo "Creating a Certificate Signing Request...
NOTE: Use a different Organization Name!"
openssl req -new -key user.key -out user.csr
echo "Signing CSR..."
openssl x509 -req -days 365 -in user.csr -CA fullchain.pem -CAkey privkey.pem -set_serial 01 -out user.crt
echo "Creating a PKCS #12 (PFX)..."
openssl pkcs12 -export -out user.pfx -inkey user.key -in user.crt -certfile fullchain.pem
echo "Verifying Certificates for mTLS..."
openssl verify -verbose -CAfile fullchain.pem user.crt
